# Build the latest OpenSSL by docker container.

### Using the Docker.io Cloud machine

[![Build status](https://gitlab.com//nguoianphu/docker-openssl/badges/master/build.svg)](https://gitlab.com//nguoianphu/docker-openssl/commits/master)
[![CE coverage report](https://gitlab.com//nguoianphu/docker-openssl/badges/master/coverage.svg?job=coverage)](http://nguoianphu.gitlab.io/docker-openssl/coverage-ruby)
[![Code Climate](https://codeclimate.com/github//nguoianphu/docker-openssl.svg)](https://codeclimate.com/github//nguoianphu/docker-openssl)


### It's really awesome!

#### OS base on Alpine 64-bit (3.4)

``` alpine:3.4 ```

### I don't know how to get the latest version of OpenSSL, like openssl-latest.tar.gz
### So I have to put the version here
## OPENSSL VERSION=```1.0.2j```

## Build and run

    docker build -t "openssl" .
    docker run --rm -it openssl openssl version

or
   
    docker run --rm -it nguoianphu/docker-openssl openssl version
